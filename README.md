# branding
CuriosityDev's images and assets

## Licensing for our assets
All of our assets are bound by [the license located in our branding GitHub repository](https://github.com/CuriosityDev/branding/raw/master/LICENSE).

**Any attempt to edit, recolor, reconfigure, or otherwise alter our logo will be taken care of swiftly by our legal team. Basically, *don't try editing our logo***. Report all cases of logo abuse to either `abuse@curiositydev.me` or `legal@curiositydev.me`, do not contact `support@curiositydev.me`, though.

## Logos and Assets
**Logos**:
- [CuriosityDev Logo 512x512 (as of April 25, 2018)](https://github.com/CuriosityDev/branding/raw/master/cdev-logo-512x512.png)

**Assets**:
- [Light bulb icon](https://github.com/CuriosityDev/branding/raw/master/cdev_lightbulb.png)


### Colors
- Main color used in our logo (hex format): **#7ec0ee**
